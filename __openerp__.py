# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


{
    'name': 'Unikulma Customer Updation',
    'version': '0.1',
    'license': 'Other proprietary',
    'category': 'Script',
    'description': 'Unikulma Customer Updation Script',
    'author': 'SprintIT',
    'maintainer': 'SprintIT',
    'website': 'http://www.sprintit.fi',
    'depends': [
        'base',
    ],
    'data': [
        'wizard/res_partner_view.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
 }
