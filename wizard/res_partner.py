# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv

class update_partner(osv.osv_memory):
    _name = "unikulma.update.partner"
    _description = "Update Partner"

    def action_update(self, cr, uid, ids, context=None):
        context = dict(context or {})
        cr.execute("select id,name from res_partner where parent_id is not null")
        count = 0
        for row in cr.dictfetchall():
            name = row['name']
            self.pool.get('res.partner').write(cr, uid, [row['id']], {'name': name})
            count = count+1
        print 'count..................',count
        return True
    
    